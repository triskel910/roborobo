﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
//using PrUtils;

public class PlayerMovement : MonoBehaviour
{
    [Header("Movement")]
    public float s_move;
    public float s_rotate;
    private Vector3 moveDirection = Vector3.zero;
    private bool cantmove = false;

    private float speedCheck;
    private Vector3 lastPosition = Vector3.zero;

    private Animator playerAnims;
    public 
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        MovePlayer();
        speedCheck = (transform.position - lastPosition).magnitude;
        lastPosition = transform.position;

    }
    void MovePlayer()
    {
       
            if (!cantmove)
            {
                CharacterController controller = GetComponent<CharacterController>();

                moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
                moveDirection = transform.TransformDirection(moveDirection);
                moveDirection *= s_move;




                controller.Move(moveDirection * Time.deltaTime);
            }

        
    }

    void Animator()
    {
        if (speedCheck > 0.0f)
            playerAnims.SetBool("Walk", true);
        else
            playerAnims.SetBool("Walk", false);
    }

}
